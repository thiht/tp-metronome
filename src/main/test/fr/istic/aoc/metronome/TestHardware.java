package fr.istic.aoc.metronome;

import fr.istic.aoc.metronome.controller.MetronomeController;
import fr.istic.aoc.metronome.hardware.*;
import fr.istic.aoc.metronome.view.ViewAdapter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Materiel.class)
public class TestHardware {
    private Materiel materiel;
    private ViewAdapter viewAdapter;
    private Horloge horloge;
    private Clavier clavier;
    private EmetteurSonore emetteurSonore;
    private Afficheur afficheur;
    private Molette molette;

    @Before
    public void setup() {
        this.materiel = mock(Materiel.class);
        this.horloge = mock(Horloge.class);
        this.clavier = mock(Clavier.class);
        this.afficheur = mock(Afficheur.class);
        this.molette = mock(Molette.class);

        PowerMockito.mockStatic(Materiel.class);
        when(Materiel.getHorloge()).thenReturn(horloge);
        when(Materiel.getClavier()).thenReturn(clavier);
        when(Materiel.getAfficheur()).thenReturn(afficheur);
        when(Materiel.getMolette()).thenReturn(molette);

        this.viewAdapter = new ViewAdapter(materiel);
        viewAdapter.setController(new MetronomeController(viewAdapter));
    }

    @Test
    public void testClavier() {
        //mock touchepressee
        //mock horloge
        when(clavier.touchePressee(0)).thenReturn(true);
        //assert afficheur
    }
}
