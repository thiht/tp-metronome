package fr.istic.aoc.metronome.hardware.adapter;

import fr.istic.aoc.metronome.Command;
import fr.istic.aoc.metronome.hardware.Materiel;
import fr.istic.aoc.metronome.hardware.Molette;
import fr.istic.aoc.metronome.view.IMetronomeView;
import fr.istic.aoc.metronome.view.components.Slider;

public class MoletteAdapter implements Poll, Slider {
    private Molette molette = Materiel.getMolette();
    private float lastPosition;
    private IMetronomeView view;

    private static final long DEFAULT_PERIOD = 500;
    private static final float DEFAULT_THRESHOLD = 0.1f;
    private static final int DEFAULT_MINTEMPO = 40;
    private static final int DEFAULT_MAXTEMPO = 340;
    private Command command;

    public MoletteAdapter(Molette molette, IMetronomeView view) {
        this.molette = molette;
        this.view = view;
        this.command = this::update;
    }

    @Override
    public void init(long period) {
        lastPosition = 0;
        horloge.activerPeriodiquement(command, period);
    }

    @Override
    public void update() {
        float position = molette.position();

        if (Math.abs(position-lastPosition) > DEFAULT_THRESHOLD) {
            setTempo((int)(position*(DEFAULT_MAXTEMPO - DEFAULT_MINTEMPO)));
        }
        lastPosition = position;
    }

    @Override
    public void close() {
        horloge.desactiver(command);
    }

    @Override
    public void init() {
        init(DEFAULT_PERIOD);
    }

    @Override
    public void setTempo(int tempo) {
        view.setTempo(tempo);
    }
}
