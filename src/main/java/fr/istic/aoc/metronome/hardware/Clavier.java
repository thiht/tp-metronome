package fr.istic.aoc.metronome.hardware;

public interface Clavier {
    /**
     *
     * @param i 1: start, 2: stop, 3: inc, 4: dec
     * @return true if pressed
     */
    boolean touchePressee(int i);

    enum Touche {
        START,
        STOP,
        INC,
        DEC
    }
}
