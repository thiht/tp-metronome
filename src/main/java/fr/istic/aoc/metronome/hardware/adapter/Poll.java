package fr.istic.aoc.metronome.hardware.adapter;

import fr.istic.aoc.metronome.hardware.Horloge;
import fr.istic.aoc.metronome.hardware.Materiel;

public interface Poll {
    Horloge horloge = Materiel.getHorloge();
    void init(long period);
    void update();
    void close();
}
