package fr.istic.aoc.metronome.hardware;

public interface EmetteurSonore {
    void emettreClic();
}
