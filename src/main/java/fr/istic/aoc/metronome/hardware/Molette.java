package fr.istic.aoc.metronome.hardware;

public interface Molette {
    /**
     *
     * @return return the position between 0.0 and 1.0
     */
    float position();
}
