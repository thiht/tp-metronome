package fr.istic.aoc.metronome.hardware.adapter;

import fr.istic.aoc.metronome.hardware.Afficheur;
import fr.istic.aoc.metronome.hardware.Horloge;
import fr.istic.aoc.metronome.hardware.Materiel;
import fr.istic.aoc.metronome.view.components.Display;

public class AfficheurAdapter implements Display {
    private static final float BLINK = 0.25f;
    private Afficheur afficheur;
    private Horloge horloge;

    public AfficheurAdapter(Afficheur afficheur, Horloge horloge) {
        this.afficheur = afficheur;
        this.horloge = horloge;
    }
    @Override
    public void setTempo(int newValue) {
        afficheur.afficherTempo(newValue);
    }

    @Override
    public void flashLEDMeasure() {
        afficheur.allumerLED(Afficheur.MEASURE_LED);
        horloge.activerApresDelai(()-> afficheur.eteindreLED(Afficheur.MEASURE_LED), BLINK);
    }

    @Override
    public void flashLEDBeat() {
        afficheur.allumerLED(Afficheur.TEMPO_LED);
        horloge.activerApresDelai(()-> afficheur.eteindreLED(Afficheur.TEMPO_LED), BLINK);
    }
}
