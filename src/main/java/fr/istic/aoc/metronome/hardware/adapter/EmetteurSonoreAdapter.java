package fr.istic.aoc.metronome.hardware.adapter;

import fr.istic.aoc.metronome.hardware.EmetteurSonore;
import fr.istic.aoc.metronome.hardware.Materiel;
import fr.istic.aoc.metronome.view.components.SoundPlayer;

public class EmetteurSonoreAdapter implements SoundPlayer {
    private EmetteurSonore emetteurSonore;

    public EmetteurSonoreAdapter(EmetteurSonore emetteurSonore) {
        this.emetteurSonore = emetteurSonore;
    }
    @Override
    public void playBeat() {
        emetteurSonore.emettreClic();
    }

    @Override
    public void playMeasure() {
    }
}
