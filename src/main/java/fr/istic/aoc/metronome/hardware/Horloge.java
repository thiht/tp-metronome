package fr.istic.aoc.metronome.hardware;

import fr.istic.aoc.metronome.Command;

public interface Horloge {
    void activerPeriodiquement(Command cmd, float periodeEnSecondes);
    void activerApresDelai(Command cmd, float delaiEnSecondes);
    void desactiver(Command cmd);
}
