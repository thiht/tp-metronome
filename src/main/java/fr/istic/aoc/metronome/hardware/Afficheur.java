package fr.istic.aoc.metronome.hardware;

public interface Afficheur {
    int TEMPO_LED = 1;
    int MEASURE_LED = 2;

    void allumerLED(int numLED);
    void eteindreLED(int numLED);

    void afficherTempo(int valeurTempo);
}
