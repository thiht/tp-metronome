package fr.istic.aoc.metronome.hardware.adapter;


import fr.istic.aoc.metronome.Command;
import fr.istic.aoc.metronome.hardware.Clavier;
import fr.istic.aoc.metronome.view.IMetronomeView;
import fr.istic.aoc.metronome.view.components.Keyboard;

import java.util.concurrent.Callable;

import static fr.istic.aoc.metronome.hardware.Clavier.Touche;

public class ClavierAdapter implements Poll, Keyboard {
    private Clavier clavier;
    private IMetronomeView view;
    private static final long DEFAULT_PERIOD = 500;
    private Command command;
    private boolean isDecreasing;
    private boolean isIncreasing;

    public ClavierAdapter(Clavier clavier, IMetronomeView view) {
        this.clavier = clavier;
        this.view = view;
        this.command = this::update;
        this.isDecreasing = false;
        this.isIncreasing = false;
        init(DEFAULT_PERIOD);
    }

    @Override
    public void init(long period) {
        horloge.activerPeriodiquement(command, period);
    }

    @Override
    public void update() {
        if (clavier.touchePressee(Touche.START.ordinal()))  {
            handleStart();
        }
        else if (clavier.touchePressee(Touche.STOP.ordinal()))  {
            handleStop();
        }

        if (clavier.touchePressee(Touche.INC.ordinal()) && !isIncreasing)  {
            handleIncBPM();
            isIncreasing = true;
        }
        else if (clavier.touchePressee(Touche.DEC.ordinal()) && !isDecreasing)  {
            handleDecBPM();
            isDecreasing = true;
        }
        else {
            isDecreasing = false;
            isIncreasing = false;
        }
    }

    @Override
    public void close() {
        horloge.desactiver(command);
    }

    @Override
    public void handleStop() {
        view.onStop();
    }

    @Override
    public void handleClose() {
        view.onClose();
    }

    @Override
    public void handleStart() {
        view.onStart();
    }

    @Override
    public void handleIncBPM() {
        view.onInc();
    }

    @Override
    public void handleDecBPM() {
        view.onDec();
    }
}
