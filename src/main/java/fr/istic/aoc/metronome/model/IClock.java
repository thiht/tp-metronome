package fr.istic.aoc.metronome.model;

import fr.istic.aoc.metronome.Command;

/**
 *
 */
public interface IClock {
    /**
     * Sets a command executed at each period
     * @param command The command to execute
     */
    void setCommand(Command command);

    /**
     * Sets the period of the clock
     * @param period The new period of the clock > 0
     */
    void setPeriod(long period);

    /**
     * Start the clock
     */
    void start();

    /**
     * Stops the clock
     */
    void stop();

    /**
     * Ends the clock definitely
     */
    void close();

    /**
     * @return true if the ScheduledExecutorService has been shut down
     */
    boolean isClosed();
}
