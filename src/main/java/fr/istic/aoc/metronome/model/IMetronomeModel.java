package fr.istic.aoc.metronome.model;

import fr.istic.aoc.metronome.Command;

/**
 * Represents the core of the metronome.
 */
public interface IMetronomeModel {
    /**
     *
     * @return The rythm of a measure in beats per minute.
     */
    int getTempo();

    /**
     * Modifies the tempo.
     * @param tempo The new tempo in beats per minute.
     * @throws IllegalArgumentException If tempo <= 0.
     */
    void setTempo(int tempo) throws IllegalArgumentException;

    /**
     *
     * @return The number of beats per measure, between 2 included and 7 included.
     */
    int getBeatsPerMeasure();

    /**
     * Modifies the number of beats per measure.
     * @param beatsPerMeasure The new number of beats per measure, between 2 included and 7 included.
     * @throws IllegalArgumentException If beatsPerMeasure < 2 or beatsPerMeasure > 7.
     */
    void setBeatsPerMeasure(int beatsPerMeasure) throws IllegalArgumentException;

    /**
     * Starts the metronome. This method does nothing if the metronome is already started.
     */
    void start();

    /**
     * Stops the metronome. This method does nothing if the metronome is already started.
     */
    void stop();

    /**
     * Stops the metronome for good.
     */
    void terminate();

    /**
     *
     * @return true if the metronome is running, false if it's not.
     */
    boolean isRunning();

    /**
     *
     */
    void setClockCommand(Command clockEvent);
}
