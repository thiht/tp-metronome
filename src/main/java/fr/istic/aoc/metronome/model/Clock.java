package fr.istic.aoc.metronome.model;

import fr.istic.aoc.metronome.Command;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * A clock that plays a command every x milliseconds.
 */
public class Clock implements IClock {

    private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture<?> beeper;
    private Command command;
    private long period;

    /**
     * Defines a new clock
     * @param command The default command to execute at each period
     * @param period The default period
     */
    public Clock(Command command, long period) {
        setCommand(command);
        setPeriod(period);
        this.beeper = scheduler.scheduleAtFixedRate(()->{}, 0, period, TimeUnit.MILLISECONDS);
    }

    /**
     * {@inheritDoc}
     */
    public void setCommand(Command command) {
        if (command == null) {
            throw new IllegalArgumentException("The command must not be null");
        }
        this.command = command;
    }

    /**
     * {@inheritDoc}
     */
    public void setPeriod(long period) {
        if (period <= 0) {
            throw new IllegalArgumentException("The period must be greater than 0");
        }
        this.period = period;
    }

    /**
     * Starts the clock
     */
    @Override
    public void start() {
        beeper = scheduler.scheduleAtFixedRate(command::execute, 0, period, TimeUnit.MILLISECONDS);
    }

    /**
     * Stops the clock
     */
    @Override
    public void stop() {
        beeper.cancel(true);
    }

    /**
     * Shutdowns the ScheduledExecutorService. The clock cannot be started again after this
     */
    @Override
    public void close() {
        scheduler.shutdown();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isClosed() {
        return scheduler.isShutdown();
    }
}
