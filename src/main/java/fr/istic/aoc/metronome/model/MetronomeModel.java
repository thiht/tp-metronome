package fr.istic.aoc.metronome.model;

import fr.istic.aoc.metronome.Command;

public class MetronomeModel implements IMetronomeModel {

    public static final int MIN_BEATS_PER_MEASURE = 2;
    public static final int MAX_BEATS_PER_MEASURE = 7;
    private static int DEFAULT_TEMPO = 160;
    private static int DEFAULT_BEATS_PER_MEASURE = 4;
    private int tempo;
    private int beatsPerMeasure;
    private boolean running;
    private IClock clock;

    /**
     * Defines a MetronomeModel with the values DEFAULT_TEMPO and DEFAULT_BEATS_PER_MEASURE
     * @see MetronomeModel#MetronomeModel(int, int)
     */
    public MetronomeModel() {
        this(DEFAULT_TEMPO, DEFAULT_BEATS_PER_MEASURE);
    }

    /**
     * Defines a MetronomeModel
     * @param tempo The default tempo of the metronome
     * @param beatsPerMeasure The default number of beats per measure of the metronome
     */
    public MetronomeModel(int tempo, int beatsPerMeasure) {
        clock = new Clock(()->{}, tempoToMilliseconds(tempo));
        setTempo(tempo);
        setBeatsPerMeasure(beatsPerMeasure);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getTempo() {
        return tempo;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setTempo(int tempo) throws IllegalArgumentException {
        if (tempo <= 0) {
            throw new IllegalArgumentException("The new tempo must be strictly greater than 0");
        }

        this.tempo = tempo;
        clock.setPeriod(tempoToMilliseconds(tempo));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getBeatsPerMeasure() {
        return beatsPerMeasure;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setBeatsPerMeasure(int beatsPerMeasure) throws IllegalArgumentException {
        if (beatsPerMeasure < MIN_BEATS_PER_MEASURE || beatsPerMeasure > MAX_BEATS_PER_MEASURE) {
            return;
        }

        this.beatsPerMeasure = beatsPerMeasure;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void start() {
        if (!running) {
            clock.start();
        }

        running = true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stop() {
        if (running) {
            clock.stop();
        }

        running = false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void terminate() {
        clock.close();
        running = false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isRunning() {
        return running;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setClockCommand(Command clockEvent) {
        clock.setCommand(clockEvent);
    }

    private long tempoToMilliseconds(int tempo) {
        return (long) ((60./tempo)*1000);
    }
}
