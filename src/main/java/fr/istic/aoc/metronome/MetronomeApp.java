package fr.istic.aoc.metronome;

import fr.istic.aoc.metronome.controller.MetronomeController;
import fr.istic.aoc.metronome.view.ViewAdapter;
import fr.istic.aoc.metronome.view.simulated.IMetronomeTestView;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Metronome App
 *
 */
public class MetronomeApp extends Application {

    private IMetronomeTestView view;

    public static void main(String[] args) {
        // launch JavaFX application
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        Parent root = loader.load(getClass().getResource("/metronome.fxml").openStream());
        view = loader.getController();
        ViewAdapter viewAdapter = new ViewAdapter(view);
        viewAdapter.setController(new MetronomeController(viewAdapter));


        stage.setTitle("Metronome");
        stage.setResizable(false);
        stage.setScene(new Scene(root, 300, 150));
        stage.show();
    }

    @Override
    public void stop() throws Exception {
        view.onClose();
        super.stop();
    }
}
