package fr.istic.aoc.metronome.controller;

public interface IMetronomeController {
    /**
     * Controls the start of the rhythmbox.
     */
    void handleStart();
    /**
     * Controls the stop of the rhythmbox.
     */
    void handleStop();

    /**
     * Controls increase of Beats per minute
     */
    void handleIncBPM();
    /**
     * Controls decrease of Beats per minute
     */
    void handleDecBPM();
    /**
     * Controls the change of the tempo
     * @param newTempo the tempo to set
     */
    void handleTempoChange(int newTempo);

    /**
     * Controls the beat event.
     */
    void handleClockEvent();

    /**
     * Controls the close event.
     */
    void handleClose();
}
