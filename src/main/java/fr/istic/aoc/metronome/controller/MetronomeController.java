package fr.istic.aoc.metronome.controller;

import fr.istic.aoc.metronome.model.MetronomeModel;
import fr.istic.aoc.metronome.view.IMetronomeView;

public class MetronomeController implements IMetronomeController {
    private final MetronomeModel engine = new MetronomeModel();
    private final IMetronomeView view;
    private static int currentBeat = 0;

    public MetronomeController(IMetronomeView view) {
        this.view = view;
        view.init();
        engine.setClockCommand(this::handleClockEvent);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void handleStart() {
        engine.start();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void handleStop() {
        engine.stop();
        currentBeat = 0;
    }

    @Override
    public void handleClose() {
        engine.terminate();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void handleIncBPM() {
        engine.setBeatsPerMeasure(engine.getBeatsPerMeasure() + 1);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void handleDecBPM() {
        engine.setBeatsPerMeasure(engine.getBeatsPerMeasure() - 1);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void handleTempoChange(int newTempo) {
        engine.setTempo(newTempo);
        view.setTempo(newTempo);
        if (engine.isRunning()) {
            engine.stop();
            engine.start();
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void handleClockEvent() {
        currentBeat %= engine.getBeatsPerMeasure();
        if (currentBeat == 0) {
            view.onMeasure();
        }
        view.onBeat();
        currentBeat++;
    }
}
