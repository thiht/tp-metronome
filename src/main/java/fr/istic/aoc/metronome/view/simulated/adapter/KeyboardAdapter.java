package fr.istic.aoc.metronome.view.simulated.adapter;

import fr.istic.aoc.metronome.view.IMetronomeView;
import fr.istic.aoc.metronome.view.components.Keyboard;

/**
 * Adapter of Keyboard interface
 */
public class KeyboardAdapter implements Keyboard {
    private IMetronomeView view;

    public KeyboardAdapter(IMetronomeView view) {
        this.view = view;
    }

    @Override
    public void handleStop() {
        view.onStop();
    }

    @Override
    public void handleClose() {
        view.onClose();
    }

    @Override
    public void handleStart() {
        view.onStart();
    }

    @Override
    public void handleIncBPM() {
        view.onInc();
    }

    @Override
    public void handleDecBPM() {
        view.onDec();
    }
}
