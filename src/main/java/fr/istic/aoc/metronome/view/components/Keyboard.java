package fr.istic.aoc.metronome.view.components;

/**
 * Interface which contains all the buttons
 */
public interface Keyboard {
    void handleStop();

    void handleClose();

    void handleStart();

    void handleIncBPM();

    void handleDecBPM();
}
