package fr.istic.aoc.metronome.view.simulated.adapter;

import fr.istic.aoc.metronome.view.components.SoundPlayer;
import javafx.scene.media.AudioClip;

/**
 * Adapter of SoundPlayer adapter
 */
public class SoundPlayerAdapter implements SoundPlayer {
    private final AudioClip soundBeat = new AudioClip(getClass().getResource("/kick.wav").toString());
    private final AudioClip soundMeasure = new AudioClip(getClass().getResource("/snare.wav").toString());

    @Override
    public void playBeat() {
        soundBeat.play();
    }

    @Override
    public void playMeasure() {
        soundMeasure.play();
    }
}
