package fr.istic.aoc.metronome.view.simulated;

import fr.istic.aoc.metronome.view.simulated.adapter.KeyboardAdapter;
import fr.istic.aoc.metronome.view.simulated.adapter.SliderAdapter;

/**
 * Interface for simulated metronome.
 */
public interface IMetronomeTestView {

    void setKeyboard(KeyboardAdapter keyboard);

    void setSlider(SliderAdapter slider);

    void init();

    void onBeat();

    void onMeasure();

    void onClose();

    void onStart();

    void onStop();

    void onInc();

    void onDec();

    void onTempoChange(int newValue);
}
