package fr.istic.aoc.metronome.view.simulated;

import fr.istic.aoc.metronome.view.simulated.adapter.KeyboardAdapter;
import fr.istic.aoc.metronome.view.simulated.adapter.SliderAdapter;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * A JavaFX interface for simulated metronome.
 */
public class MetronomeTestView implements IMetronomeTestView {
    private KeyboardAdapter keyboard;
    private SliderAdapter sliderAdapter;

    @FXML
    private Circle measureLed;

    @FXML
    private Circle beatLed;

    @FXML
    private Slider tempoSlider;

    @FXML
    private Label tempoLabel;

    private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();


    @Override
    public void init() {
        tempoSlider.valueProperty().addListener((observableValue, oldValue, newValue) -> {
            sliderAdapter.setTempo(newValue.intValue());
        });
    }

    @Override
    public void setKeyboard(KeyboardAdapter keyboard) {
        this.keyboard = keyboard;
    }

    @Override
    public void setSlider(SliderAdapter slider) {
        this.sliderAdapter = slider;
    }

    @Override
    public void onBeat() {
        beatLed.setFill(Color.DARKSEAGREEN);
        scheduler.schedule(() -> beatLed.setFill(Color.CRIMSON), 250, TimeUnit.MILLISECONDS);
    }

    @Override
    public void onMeasure() {
        measureLed.setFill(Color.DARKSEAGREEN);
        scheduler.schedule(() -> measureLed.setFill(Color.CRIMSON), 250, TimeUnit.MILLISECONDS);
    }

    @Override
    public void onClose() {
        keyboard.handleStop();
        keyboard.handleClose();
        scheduler.shutdown();
    }

    @Override
    public void onStart() {
        keyboard.handleStart();
    }

    @Override
    public void onStop() {
        keyboard.handleStop();
    }

    @Override
    public void onInc() {
        keyboard.handleIncBPM();
    }

    @Override
    public void onDec() {
        keyboard.handleDecBPM();
    }

    @Override
    public void onTempoChange(int newValue) {
        tempoLabel.setText(String.valueOf(newValue));
    }
}