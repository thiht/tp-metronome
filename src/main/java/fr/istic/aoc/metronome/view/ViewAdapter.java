package fr.istic.aoc.metronome.view;


import fr.istic.aoc.metronome.controller.IMetronomeController;
import fr.istic.aoc.metronome.hardware.Materiel;
import fr.istic.aoc.metronome.hardware.adapter.AfficheurAdapter;
import fr.istic.aoc.metronome.hardware.adapter.ClavierAdapter;
import fr.istic.aoc.metronome.hardware.adapter.EmetteurSonoreAdapter;
import fr.istic.aoc.metronome.hardware.adapter.MoletteAdapter;
import fr.istic.aoc.metronome.view.components.*;
import fr.istic.aoc.metronome.view.simulated.IMetronomeTestView;
import fr.istic.aoc.metronome.view.simulated.adapter.DisplayAdapter;
import fr.istic.aoc.metronome.view.simulated.adapter.KeyboardAdapter;
import fr.istic.aoc.metronome.view.simulated.adapter.SliderAdapter;
import fr.istic.aoc.metronome.view.simulated.adapter.SoundPlayerAdapter;

/**
 * Adapts the controller to the view interface
 */
public class ViewAdapter implements IMetronomeView {
    private IMetronomeController ctrl;

    private Display display;
    private Slider sliderAdapter;
    private SoundPlayer soundPlayer;

    /**
     * Builds a simulated view
     * @param testView the simulated view
     */
    public ViewAdapter(IMetronomeTestView testView) {
        this.display = new DisplayAdapter(testView);
        this.sliderAdapter = new SliderAdapter(this, testView);
        testView.setSlider((SliderAdapter) sliderAdapter);
        this.soundPlayer = new SoundPlayerAdapter();
        testView.setKeyboard(new KeyboardAdapter(this));
    }

    /**
     * Builds a hardware view
     * @param materiel Tell that it must build the hardware interface
     */
    public ViewAdapter(Materiel materiel) {
        display = new AfficheurAdapter(Materiel.getAfficheur(), Materiel.getHorloge());
        sliderAdapter = new MoletteAdapter(Materiel.getMolette(), this);
        soundPlayer = new EmetteurSonoreAdapter(Materiel.getEmetteurSonore());
        new ClavierAdapter(Materiel.getClavier(), this);
    }

    @Override
    public void init() {
        sliderAdapter.init();
    }

    @Override
    public void setController(IMetronomeController ctrl) {
        this.ctrl = ctrl;
    }

    @Override
    public void onBeat() {
        soundPlayer.playBeat();
        display.flashLEDBeat();
    }

    @Override
    public void onMeasure() {
        soundPlayer.playMeasure();
        display.flashLEDMeasure();
    }

    @Override
    public void onClose() {
        ctrl.handleStop();
        ctrl.handleClose();
    }

    @Override
    public void onStart() {
        ctrl.handleStart();
    }

    @Override
    public void onStop() {
        ctrl.handleStop();
    }

    @Override
    public void onInc() {
        ctrl.handleIncBPM();
    }

    @Override
    public void onDec() {
        ctrl.handleDecBPM();
    }

    @Override
    public void onTempoChange(int newValue) {
        ctrl.handleTempoChange(newValue);
    }

    @Override
    public void setTempo(int newValue) {
        display.setTempo(newValue);
    }
}
