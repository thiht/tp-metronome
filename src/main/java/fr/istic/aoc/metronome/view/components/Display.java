package fr.istic.aoc.metronome.view.components;

/**
 * Interface which controls the informations to display.
 */
public interface Display {
    void setTempo(int newValue);
    void flashLEDMeasure();
    void flashLEDBeat();
}
