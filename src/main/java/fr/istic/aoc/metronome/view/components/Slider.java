package fr.istic.aoc.metronome.view.components;

/**
 * Interface for a slider that controls the tempo
 */
public interface Slider {
    /**
     * Initializes the polling on the slider value.
     */
    void init();

    /**
     * Command that transmit the tempo to the controller.
     * @param tempo the value to transmit
     */
    void setTempo(int tempo);
}
