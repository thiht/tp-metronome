package fr.istic.aoc.metronome.view.simulated.adapter;

import fr.istic.aoc.metronome.view.IMetronomeView;
import fr.istic.aoc.metronome.view.components.Slider;
import fr.istic.aoc.metronome.view.simulated.IMetronomeTestView;

/**
 * Adapter of Slider interface
 */
public class SliderAdapter implements Slider {
    private IMetronomeView view;
    private IMetronomeTestView javaFXView;

    public SliderAdapter(IMetronomeView view,IMetronomeTestView testView) {
        this.javaFXView = testView;
        this.view = view;
    }

    @Override
    public void init() {
        javaFXView.init();
    }

    @Override
    public void setTempo(int tempo) {
        view.onTempoChange(tempo);
    }

}
