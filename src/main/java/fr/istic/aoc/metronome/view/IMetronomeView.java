package fr.istic.aoc.metronome.view;

import fr.istic.aoc.metronome.controller.IMetronomeController;

/**
 *
 * Interface to adapt the controller to the components.
 */
public interface IMetronomeView {
    /**
     * Initializes the listeners
     */
    void init();

    void setController(IMetronomeController ctrl);

    void onBeat();

    void onMeasure();

    void onClose();

    void onStart();

    void onStop();

    void onInc();

    void onDec();

    /**
     * Transmit the new tempo coming from the slider
     * @param newValue tempo to give to the controller
     */
    void onTempoChange(int newValue);

    /**
     * Set the tempo on the display
     * @param newValue the new tempo to display
     */
    void setTempo(int newValue);
}
