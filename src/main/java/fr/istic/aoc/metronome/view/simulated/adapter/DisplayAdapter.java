package fr.istic.aoc.metronome.view.simulated.adapter;

import fr.istic.aoc.metronome.view.components.Display;
import fr.istic.aoc.metronome.view.simulated.IMetronomeTestView;

/**
 * Adapter of Display interface
 */
public class DisplayAdapter implements Display {
    private IMetronomeTestView javaFXView;

    public DisplayAdapter(IMetronomeTestView testView) {
        this.javaFXView = testView;
    }

    @Override
    public void setTempo(int newValue) {
        javaFXView.onTempoChange(newValue);
    }

    @Override
    public void flashLEDMeasure() {
        javaFXView.onMeasure();
    }

    @Override
    public void flashLEDBeat() {
        javaFXView.onBeat();
    }
}
