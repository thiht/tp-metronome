package fr.istic.aoc.metronome.view.components;

/**
 * Interface to play the sounds of the metronome
 */
public interface SoundPlayer {
    void playBeat();
    void playMeasure();
}
