package fr.istic.aoc.metronome;

public interface Command {
    void execute();
}
