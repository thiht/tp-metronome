# AOC - TP Métronome
Thibaut Rousseau

Thibaud Hulin

![Moteur - Diagramme de classes](docs/td2-moteur-diagramme-classes.png)

![Observer - Diagramme de classes](docs/td2-observer-diagramme-classes.png)

![Moteur - Initialisation - Diagramme de séquence](docs/td2-moteur-diagramme-sequence-initialisation.png)

![Moteur - Exécution - Diagramme de séquence](docs/td2-moteur-diagramme-sequence-execution.png)